﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace SmartFlow.views
{
    /// <summary>
    /// Interaction logic for StatiscBar.xaml
    /// </summary>
    public partial class StatiscBar : Page
    {
        public StatiscBar()
        {
            InitializeComponent();
            this.DataContext = this;

            Data = new ObservableCollection<MyData>();
            Data.Add(new MyData() { Year = 2006, Value = 50.5, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2006, Value = 46.2, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2006, Value = 34.7, WorkType = WorkTypes.Receipt });
            Data.Add(new MyData() { Year = 2006, Value = 32, WorkType = WorkTypes.Draft });
            Data.Add(new MyData() { Year = 2007, Value = 13.41, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2008, Value = 20.0, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2008, Value = 21.25, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2009, Value = 5.5, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2010, Value = 12.5, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2010, Value = 10.25, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2010, Value = 18, WorkType = WorkTypes.Receipt });
            Data.Add(new MyData() { Year = 2010, Value = 25, WorkType = WorkTypes.Draft });
            Data.Add(new MyData() { Year = 2011, Value = 18.0, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2011, Value = 15.3, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2011, Value = 19.2, WorkType = WorkTypes.Receipt });
            Data.Add(new MyData() { Year = 2011, Value = 10, WorkType = WorkTypes.Draft });
            Data.Add(new MyData() { Year = 2012, Value = 22.0, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2012, Value = 17.5, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2013, Value = 19.8, WorkType = WorkTypes.Buy });
            Data.Add(new MyData() { Year = 2013, Value = 20.0, WorkType = WorkTypes.Sell });
            Data.Add(new MyData() { Year = 2013, Value = 26.0, WorkType = WorkTypes.Receipt });
        }

        private ObservableCollection<MyData> _data = null;
        public ObservableCollection<MyData> Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                Notify("Data");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }


    public enum WorkTypes
    {
        Buy,
        Sell,
        Receipt,
        Draft
    }

    public class MyData
    {
        public MyData()
        {
        }

        public int Year { get; set; }

        public double Value { get; set; }

        public WorkTypes WorkType { get; set; }
    }

    #region Converter
    public class Bool2Visibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
                return (Visibility)value == Visibility.Visible ? true : false;
            else
                throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return (bool)value ? Visibility.Visible : Visibility.Collapsed;
            else
                throw new NotImplementedException();
        }
    }
    #endregion Converter
}
